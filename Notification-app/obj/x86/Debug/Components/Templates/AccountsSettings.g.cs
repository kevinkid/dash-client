﻿#pragma checksum "..\..\..\..\..\Components\Templates\AccountsSettings.xaml" "{406ea660-64cf-4c82-b6f0-42d48172a799}" "FAF88FDFEA5861A30D097555803F991C"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using Dash.Components.Templates;
using MaterialDesignThemes.Wpf;
using MaterialDesignThemes.Wpf.Transitions;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace Dash.Components.Templates {
    
    
    /// <summary>
    /// Accounts
    /// </summary>
    public partial class Accounts : System.Windows.Controls.UserControl, System.Windows.Markup.IComponentConnector {
        
        
        #line 44 "..\..\..\..\..\Components\Templates\AccountsSettings.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox ChoosenAccount;
        
        #line default
        #line hidden
        
        
        #line 50 "..\..\..\..\..\Components\Templates\AccountsSettings.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBoxItem gmail;
        
        #line default
        #line hidden
        
        
        #line 53 "..\..\..\..\..\Components\Templates\AccountsSettings.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBoxItem yammer;
        
        #line default
        #line hidden
        
        
        #line 56 "..\..\..\..\..\Components\Templates\AccountsSettings.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBoxItem skype;
        
        #line default
        #line hidden
        
        
        #line 59 "..\..\..\..\..\Components\Templates\AccountsSettings.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBoxItem outlook;
        
        #line default
        #line hidden
        
        
        #line 63 "..\..\..\..\..\Components\Templates\AccountsSettings.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button AddAccountBtn;
        
        #line default
        #line hidden
        
        
        #line 76 "..\..\..\..\..\Components\Templates\AccountsSettings.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Image image;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/Notification_app;component/components/templates/accountssettings.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\..\..\..\Components\Templates\AccountsSettings.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.ChoosenAccount = ((System.Windows.Controls.ComboBox)(target));
            
            #line 46 "..\..\..\..\..\Components\Templates\AccountsSettings.xaml"
            this.ChoosenAccount.DropDownOpened += new System.EventHandler(this.ChoosenAccount_DropDownOpened);
            
            #line default
            #line hidden
            
            #line 47 "..\..\..\..\..\Components\Templates\AccountsSettings.xaml"
            this.ChoosenAccount.SelectionChanged += new System.Windows.Controls.SelectionChangedEventHandler(this.ChoosenAccount_Selected);
            
            #line default
            #line hidden
            return;
            case 2:
            this.gmail = ((System.Windows.Controls.ComboBoxItem)(target));
            
            #line 50 "..\..\..\..\..\Components\Templates\AccountsSettings.xaml"
            this.gmail.Selected += new System.Windows.RoutedEventHandler(this.gmail_Selected);
            
            #line default
            #line hidden
            return;
            case 3:
            this.yammer = ((System.Windows.Controls.ComboBoxItem)(target));
            
            #line 53 "..\..\..\..\..\Components\Templates\AccountsSettings.xaml"
            this.yammer.Selected += new System.Windows.RoutedEventHandler(this.yammer_Selected);
            
            #line default
            #line hidden
            return;
            case 4:
            this.skype = ((System.Windows.Controls.ComboBoxItem)(target));
            
            #line 56 "..\..\..\..\..\Components\Templates\AccountsSettings.xaml"
            this.skype.Selected += new System.Windows.RoutedEventHandler(this.skype_Selected);
            
            #line default
            #line hidden
            return;
            case 5:
            this.outlook = ((System.Windows.Controls.ComboBoxItem)(target));
            
            #line 59 "..\..\..\..\..\Components\Templates\AccountsSettings.xaml"
            this.outlook.Selected += new System.Windows.RoutedEventHandler(this.outlook_Selected);
            
            #line default
            #line hidden
            return;
            case 6:
            this.AddAccountBtn = ((System.Windows.Controls.Button)(target));
            
            #line 63 "..\..\..\..\..\Components\Templates\AccountsSettings.xaml"
            this.AddAccountBtn.Click += new System.Windows.RoutedEventHandler(this.AddAccountBtn_Click);
            
            #line default
            #line hidden
            return;
            case 7:
            this.image = ((System.Windows.Controls.Image)(target));
            return;
            }
            this._contentLoaded = true;
        }
    }
}

