﻿using System;
using System.Web;
using System.Linq;
using System.Media;
using System.Windows;
using System.Net.Http;
using Dash;
using System.Threading;
using System.Diagnostics;
using System.Configuration;
using System.Windows.Media;
using System.Windows.Shapes;
using System.Windows.Controls;
using System.Windows.Threading;
using System.Windows.Navigation;
using System.Collections.Generic;
using System.Windows.Media.Imaging;
using System.Text.RegularExpressions;
using System.Windows.Media.Animation;
using Microsoft.AspNet.SignalR.Client;




namespace Dash
{

    /// <summary>
    /// Application Interface Interaction logic
    /// </summary>
    public partial class MainWindow : Window
    {
        private double screenWResolution = System.Windows.SystemParameters.PrimaryScreenWidth;
        private double screenHResolution = System.Windows.SystemParameters.PrimaryScreenHeight;

        private List<System.Windows.Window> WinHandles;/// TODO: use this find all the windows there apps we want our app on top of.
        private System.Windows.Window winHandles;
        private static DispatcherTimer ActiveWindowChecker;
        private static DispatcherTimer ClockTimer;

        public Dictionary<string, string> client = new Dictionary<string, string>();
        public IHubProxy HubProxy { set; get; }

        //const string ServerURI = "https://dashdesk.azurewebsites.net";
        private const string ServerURI = "http://localhost:1337";

        public HubConnection HubConnect { set; get; }

        public bool signedin = false;
        private System.Windows.Window Authwindow;
        private WebBrowser AuthBrowser = new WebBrowser();


        ConnectionHandler connectionHandler = new ConnectionHandler();
        ClientSettings settings = new ClientSettings();

        private Storyboard _storyboard;
        public const double _originalHeight = 265.958;
        public bool IsSignedin { get; set; }
        public string SessionKey { get; set; }
        public double _tarHeight { get; set; }
        public double _toggleHeight { get; set; }
        public static Brush Urgency { get; set; }
        public string InNotificationUser { get; set; }
        public string InNotificationMessage { get; set; }
        public Thread DisplayNotificationOP { get; set; }
        public ListBoxItem NotificationTemplate { get; set; }
        public Dictionary<string, string> NotificationObj { get; set; }

        private MainView mainView;

        public MainWindow()
        {
            InitializeComponent();
            // ---
            //TODO: Remove me 
            //ClientSettings settings = new ClientSettings();
            //settings.SetAppSettings("SessionKey", "One");
            //---

            HubConnect = new HubConnection(ServerURI);
            HubConnect.Closed += HubClosedConnection;
            HubProxy = HubConnect.CreateHubProxy("MyHub");//[DashHubProxy] note:this should match on the server side .
            HubConnect.Credentials = System.Net.CredentialCache.DefaultCredentials;
            HubConnect.Reconnected += OnReconnect;
            HubConnect.Received += OnIncomingNotification;
            this.InNotificationUser = "@bigkevzs- twitter";
            this.InNotificationMessage = "This is a default diplay notification message it can be as long as you want.";

            //SignalR Debug output
            HubConnect.TraceLevel = TraceLevels.All;
            HubConnect.TraceWriter = Console.Out;

            connectionHandler.InitiateConnectivityPingPoll();

            InitConnectionSync();

            ClockTimer = new DispatcherTimer(DispatcherPriority.SystemIdle);
            ClockTimer.Tick += UpdateClock;
            ClockTimer.Interval = TimeSpan.FromSeconds(1);
            ClockTimer.Start();

            SoundPlayer player = new SoundPlayer();

            bool isLargeScreen = ((screenHResolution > 1200) || (screenWResolution < 1500)) ? !(SmallScreen()) : LargeSCreen();

            // --
            DoubleAnimation da = new DoubleAnimation();
            da.From = 1;
            da.To = 0;
            da.Duration = new Duration(TimeSpan.FromSeconds(2));
            da.AutoReverse = true;
            da.RepeatBehavior = RepeatBehavior.Forever;
            rectangle1.BeginAnimation(OpacityProperty, da);
            // --
        }

        /// Meduim Screen Size
        private bool SmallScreen()
        {
            this.Left = screenWResolution - screenWResolution / 2 + 220;// was 80
            this.Top = screenHResolution - screenHResolution + 20;
            return true;
        }

        /// Larger Screen Size
        private bool LargeSCreen()
        {
            this.Left = screenWResolution - screenWResolution / 3 + 80;
            this.Top = screenHResolution - screenHResolution + 20;
            return false;
        }

        
        /// <summary>
        /// Time and weather changes
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void UpdateClock(object sender, EventArgs e)
        {
            //TODO: Use internet time .
            string time = DateTime.Now.ToString("h:mm:ss tt");
            string TimeOfDay = (time.IndexOf("PM") > 0) ? "PM" : "AM";

            string day = DateTime.Today.DayOfWeek.ToString();
            string date = DateTime.Now.ToString("dd");
            string month = DateTime.Now.ToString("MMMM");
            string year = DateTime.Now.ToString("yyyy");

            time = time.Remove((time.IndexOf("PM") > 0) ? time.IndexOf("PM") - 4 : time.IndexOf("AM") - 4, 6);
            label.Content = time;
            label1.Content = TimeOfDay;
            string DateFinal = "";

            DateFinal += date + "  ";
            DateFinal += month + " ";
            DateFinal += year;

            label5.Content = day;
            label6.Content = DateFinal;
        }



        private string NotificationParser(string rawStr)
        {
            ///TODO: Write a better parser for emails with images and other elements in the body.
            return Regex.Replace(rawStr, "<.*?>", String.Empty);
        }

        /// <summary>
        /// Session connection
        /// </summary>
        private async void InitConnectionSync()
        {
            HubProxy.On<string, string>("AddMessage", (Name, Message) =>
            {
                this.InNotificationUser = Name;
                this.InNotificationMessage = Message;
                //UrgencyColorMapper(NotificationObj);// Define urgency
                DisplayNotificationOP = new Thread(
                    (delegate ()
                    {
                        SynchronizationContext.SetSynchronizationContext(new DispatcherSynchronizationContext(Dispatcher.CurrentDispatcher));
                        NotificationList.Dispatcher.Invoke(
                            (delegate ()
                            {
                                NotificationTemplate = NotificationConstructor(Name, NotificationParser(Message));
                                SoundPlayer audio = new SoundPlayer(new Uri("C:/Users/kevin/Documents/Visual Studio 2015/Projects/Notification-app-trial/Notification-app/Notification-app/Resources/sounds/notifysound.wav", UriKind.RelativeOrAbsolute).ToString()); // here WindowsFormsApplication1 is the namespace and Connect is the audio file name
                                audio.Play();
                                NotificationList.Items.Insert(0, NotificationTemplate);
                            }),
                           DispatcherPriority.Normal);
                    }))
                {
                    IsBackground = true
                };
                DisplayNotificationOP.SetApartmentState(ApartmentState.STA);
                DisplayNotificationOP.Start(); /// TODO: Dispose of the thread
                               
                Debug.WriteLine("Incoming Notification !");
                Debug.WriteLine("Name: " + Name + ", Message: " + Message);
            });
            ///TODO: Handler reconnections just incase you have expeptions .
            try
            {
                await HubConnect.Start();
            }
            catch (HttpRequestException Ex)
            {
                Debug.WriteLine("Connection Error !");
                Debug.WriteLine("Error: " + Ex);
                ServerReconnectAsync();
            }
        }

        private void OnIncomingNotification(string obj)
        {
            //TODO: Verify the user sending the notification .
        }

        private void HubClosedConnection()
        {
            Debug.WriteLine("Hub connection closed .");
        }

        public Brush UrgencyColorMapper(Dictionary<string, string> notificationObj)
        {
            /*----------------------------------------
             ----------------- [Map] -----------------
             -----------------------------------------
             OrangeRed -> urgent
             Coral -> important
             Goldenrod -> little importance
             GreenYellow
             LawnGreen
             LimeGreen
             LightGreen
             DodgerBlue
             MediumSpringGreen
             Khaki /yellow -> almost important
             Orange -> equal to important with issues

             Magenta/Orchid -> fun social
             Gray/DimGray -> not important at all
             -------------------------------------------*/
            ///TODO: Check UrgencyColorMapper aganist a set of defined rules to flag a notification as urgent or not .
            ///TODO: Make the rules settings be user definable .
            Urgency = Brushes.Cornsilk;

            return Urgency;
        }

        private static string currentTime()
        {
            string _time = "";
            _time += string.Format(@"{0}:",DateTime.Now.Hour);
            _time += string.Format(@"{0}",DateTime.Now.Minute);
            Debug.WriteLine(_time);
            return _time;
        }


        /*

         <ListBoxItem Foreground="White">
            <Border>
                <Grid Height="49" Width="506">
                    <Image Source="Resources/Icons/Gmail-icon-new.ico" Margin="" />
                    <TextBlock VerticalAlignment="Center" HorizontalAlignment="Left" Style="{StaticResource MaterialDesignBody2TextBlock}" Height="18" Margin="69,0,0,31"><Run Text="john doe - bigkevin2682@gmail.com"/></TextBlock>
                    <TextBlock  VerticalAlignment="Center" HorizontalAlignment="Left" Height="" Width="" Margin=""><Run Text="This is some email message body notification ."/></TextBlock>
                    <materialDesign:PackIcon x:Uid="attachment" Visibility="Hidden" Kind="Attachment" Width="16"  Height="18" RenderTransformOrigin="0.439,0.612" Margin="343,-1,0,0" >
                        <materialDesign:PackIcon.RenderTransform>
                            <TransformGroup>
                                <ScaleTransform/>
                                <SkewTransform/>
                                <RotateTransform Angle="-89.414"/>
                                <TranslateTransform X="2.981" Y="-1.019"/>
                            </TransformGroup>
                        </materialDesign:PackIcon.RenderTransform>
                    </materialDesign:PackIcon>
                    <TextBlock  VerticalAlignment="Center" HorizontalAlignment="Left" Margin="337,23,0,10"><Run Text="2hrs ago"/></TextBlock>
                    <Separator Foreground="White" Style="{StaticResource MaterialDesignLightSeparator}" Margin="" Height="Auto"/>
                </Grid>
            </Border>
        </ListBoxItem>

            */

        private static ListBoxItem NotificationConstructor(string headingStr, string messageStr)
        {

            var border = new Border();
            var grid = new Grid() {
                Height = 49,
                Width = 506
            };
   
            var avatar = new Image()
            {
                Source = new BitmapImage(new Uri("Resources/icons/outlook-icon.png", UriKind.RelativeOrAbsolute)),
                Margin = new Thickness(10, 0, 442, 21)
            };

            var heading = new TextBlock()
            {
                HorizontalAlignment = HorizontalAlignment.Left,
                VerticalAlignment = VerticalAlignment.Center,
                Height = 18,
                FontFamily = new FontFamily("Resources/fonts/body/navis.ttf"),
                Text = headingStr,
                Foreground = Brushes.White,
                FontStyle = new FontStyle(),
                TextDecorations = null,
                Margin = new Thickness(69, 0, 0, 31),
                FontWeight = FontWeights.DemiBold,
            };

            var body = new TextBlock()
            {
                HorizontalAlignment = HorizontalAlignment.Left,
                VerticalAlignment = VerticalAlignment.Center,
                Width = 333,
                Height = 24,
                FontFamily = new FontFamily("Resources/fonts/body/navis.ttf"),
                Text = messageStr,
                Foreground = (Brush)(new BrushConverter().ConvertFrom("#7FFFFFFF")),
                FontStyle = FontStyles.Normal,
                TextDecorations = null,
                Margin = new Thickness(69, 15, 0, 10),
                FontWeight = FontWeights.Normal,
                LineHeight = 10.667,
                TextWrapping = TextWrapping.Wrap
            };

            var recieved = new TextBlock()
            {
                FontFamily = new FontFamily("Resources/fonts/body/navis.ttf"),
                Text = (string)currentTime(),
                Foreground = (Brush)(new BrushConverter().ConvertFrom("#33FFFFFF")),
                FontStyle = FontStyles.Italic,
                TextDecorations = null,
                Margin = new Thickness(337, 23, 0, 10),
                FontWeight = FontWeights.Normal
            };
            var separatorStyle = new Style()
            {
                ///TODO: Import styles from the materialdesign seperator 
            };
            var separator = new Separator()
            {
                Foreground = Brushes.White,
                Height = 0,
                Margin = new Thickness(58, 39, 178, 0),
                Style = separatorStyle

            };
            

            var closeImg = new Image()
            {
                Source = new BitmapImage(new Uri("/Resources/images/new/close-notification-icon.png", UriKind.RelativeOrAbsolute)),
                Margin = new Thickness(480, 0, 2, 323),
                OpacityMask = (Brush)(new BrushConverter().ConvertFrom("#4C000000"))
            };
            

            grid.Children.Add(avatar);
            grid.Children.Add(heading);
            grid.Children.Add(body);
            grid.Children.Add(recieved);
            grid.Children.Add(closeImg);
            border.Child = grid;
            

            return new ListBoxItem() { Foreground = Brushes.White, Content = border };

        }

        public void AlertNotification(string key, string message)
        {
            SoundPlayer audio = new SoundPlayer(new Uri("C:/Users/kevin/Documents/Visual Studio 2015/Projects/Notification-app-trial/Notification-app/Notification-app/Resources/sounds/notifysound.wav", UriKind.Relative).ToString()); // here WindowsFormsApplication1 is the namespace and Connect is the audio file name
            audio.Play();
            NotificationList.Items.Insert(0, NotificationTemplate);
        }

        private void OfflineHandler()
        {
            ///TODO: Handle  offline state of an application .
        }

        private void OnReconnect()
        {
            // Recheck credentials
        }

        // Retry connection to the server .
        private async void ServerReconnectAsync()
        {
            Debug.WriteLine("Reconnecting ... ");
            try
            {
                InitConnectionSync();
            }
            catch (Exception ConnectionEx)
            {
                Debug.WriteLine("Connection Timeout !");
                InitConnectionSync();
            }
        }


        /// <summary>
        /// Retreave the subscription details from redirect url
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void captureSessionId(object sender, NavigationEventArgs e)
        {
            if (e.Uri.PathAndQuery.Contains("subscriptionId="))
            {
                string rURL = e.Uri.PathAndQuery;
                Authwindow.Hide();
                rURL = rURL.Remove(0, rURL.IndexOf("subscriptionId=") + 5);
                rURL = rURL.Remove(rURL.IndexOf("&userId="), rURL.Length - rURL.IndexOf("&userId="));
                Application.Current.Properties.Add("sessionKey", rURL);
            }
        }

        /*
            Event Handlers
        */

        //Close/minimise button
        private void minimizeBtn_Click(object sender, RoutedEventArgs e)
        {
            /// TODO: Minimize application and initiate background process
            Application.Current.Shutdown();
        }

        // Show Desktop button
        private void Show_Alert_click(object sender, RoutedEventArgs e)
        {
            BitmapImage MsgIcon = new BitmapImage();
            MsgIcon.UriSource = new Uri("Resources/icons/twitter-icon.png", UriKind.Relative);
        }

        //Hide Desktop button
        private void Hide_Alert_Click(object sender, RoutedEventArgs e)
        {
        }

        // show notification button
        private void button2_Click(object sender, RoutedEventArgs e)
        {
            NotificationConstructor("@bigkevzs -twitter ", "Hey, how is the project going .");
        }

        // Login button
        private void messageBtn_Click(object sender, RoutedEventArgs e)
        {
            SessionManager signin = new SessionManager();
            if (!signedin)
            {
                signin.CreateSiginBrowser("outlook");/// Default is outlook sigin 
            }
            else
            {
                /// TODO: get the authentication token and update the UI
                signin.ResetCredentialSync();
            }
        }



        // test notification
        private void callBtn_Click(object sender, RoutedEventArgs e)
        {
            NotificationTemplate = NotificationConstructor(this.InNotificationUser, this.InNotificationMessage);
            AlertNotification(this.InNotificationUser, this.InNotificationMessage);

            //----
            //ClientSettings settings = new ClientSettings();
            //ClientSettings.GetAppSettings();
            //settings.SetAppSettings("SessionKey", "Two");
            //ClientSettings.GetAppSettings();
            //Debug.WriteLine(ClientSettings.GetAppSettings());
            /// <debug-break> Check for application settings persistence . </debug-break>
            ConnectionHandler offlineHandler = new ConnectionHandler();
            offlineHandler.InitiateConnectivityPingPoll();

            // ----

        }


        private double _toggleHeightVal()
        {
            _toggleHeight = ((_originalHeight + 230) < _toggleHeight + 1) ? _originalHeight : (this.ActualHeight + 230);
            return _toggleHeight + 1;
        }

        private void ToogleHeightAnimation()
        {
            _storyboard = new Storyboard();
            DoubleAnimation _dHeightAnimation = new DoubleAnimation();
            _dHeightAnimation.From = this.ActualHeight;
            _dHeightAnimation.To = _toggleHeightVal();
            _dHeightAnimation.Duration = new Duration(TimeSpan.FromMilliseconds(300));
            _dHeightAnimation.AccelerationRatio = 1;
            Storyboard.SetTarget(_dHeightAnimation, this);
            Storyboard.SetTargetProperty(_dHeightAnimation, new PropertyPath(Window.HeightProperty));
            _storyboard.Children.Add(_dHeightAnimation);
            _storyboard.Begin();
        }


        private void settingsBtn_Click(object sender, RoutedEventArgs e)
        {
            MainView MainSettingsView = new MainView();
            MainSettingsView.ResizeMode = ResizeMode.NoResize;
            MainSettingsView.WindowStartupLocation = WindowStartupLocation.CenterScreen;
            MainSettingsView.Width = 800;
            MainSettingsView.Height = 500;
            MainSettingsView.Show();
        }

        private void ToogleArrow_Click(object sender, RoutedEventArgs e)
        {
            ToogleHeightAnimation();
        }

        public string testing_test()
        {
            return "a name ";
        }

    }
}