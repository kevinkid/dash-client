﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using System.Net.NetworkInformation;
using System.Net;
using System.Runtime.CompilerServices;
using System.Windows;
using System.ComponentModel;
using System.Threading;

namespace Dash
{
    class ConnectionHandler
    {

        Ping PingPoll;
        Thread pingThread;
        PingReply PingPollReply;
        ClientSettings settings = new ClientSettings();
        public static event PropertyChangedEventHandler PropertyChanged;

        public static string connectivity;
        public static int PingPollPause { get; set; }
        public string PingAddress { get; set; }
        private static bool Connectivity;
        public static bool Connected
        {
            get
            {
                return Connectivity;
            }
            set
            {
                if (PropertyChanged != null && Connected != value)
                    Connectivity = value;
                OnPropertyChangedMethod(value);
            }
        }




        /// <summary>
        /// Update application connectivity state and interface .
        /// </summary>
        /// <param name="Status"></param>
        protected static void OnPropertyChangedMethod(bool Status)
        {
            PropertyChangedEventHandler PropertyChangedHandler = PropertyChanged;
            if (PropertyChangedHandler != null)
                PropertyChangedHandler(Connected,
                    new PropertyChangedEventArgs((Status) ? "Connected" : "Not-connected"));///TODO: Change this to connected otherwise data binding might fail. no idea what that is .
            ChangeAppState(Status);
        }



        /// <summary> Changes the application state from connected to offline </summary>
        /// <param name="state">todo: describe state parameter on ChangeAppState</param>
        public static void ChangeAppState(bool state)
        {
            ///TODO: Implement static app settings for keeping settings used only during runtime .
            if (state != null) /// todo: check is unasigned returns a null or undefined 
            {
                if (Connected)
                {
                    Debug.WriteLine("Internet connection established .");
                    ClientSettings settings = new ClientSettings();
                    settings.SetAppSettings("LastConnectivityStatus", Convert.ToString(state));
                }
                else
                {
                    Debug.WriteLine("Internet connection lost .");
                }
            }
        }



        /// <summary>
        /// Ping Ip Address for internet connectivity check .
        /// </summary>
        /// <returns> String ip Address of closest google server ip address .</returns>
        public string IpLookupSync()
        {
            if (this?.PingAddress != null)
            {
                PingAddress = Convert.ToString((Application.Current.Properties as IEnumerable<IDictionary<string, string>>)///Note: This might not be a good idea since its not a ping request, google ping request route to the nearest server . 
                              .Where(key => key.ContainsKey("PingIp")));
            }
            else
            {
                PingAddress = Convert.ToString(Dns.GetHostAddresses("google.com")[0]);
                settings.SetAppSettings("LastTimeConnected", (string)nameof(Connected));
                Application.Current.Properties.Add("PingIp", $"{nameof(PingAddress)}");
            }
            return PingAddress;
        }
        

        /// <summary>
        /// Polls ping request to determine internet connectivity change connectivity status
        /// https://msdn.microsoft.com/en-us/library/system.runtime.compilerservices.callermembernameattribute(v=vs.110).aspx
        /// </summary>
        /// <param name="callee"></param>
        /// <param name="file">todo: describe file parameter on InternetConnectionPollSync</param>
        /// <param name="sourceLine">todo: describe sourceLine parameter on InternetConnectionPollSync</param>
        private async void InternetConnectionPollSyncAsync(
            [System.Runtime.CompilerServices.CallerMemberName] string callee = "",
            [System.Runtime.CompilerServices.CallerFilePath] string file = "",
            [System.Runtime.CompilerServices.CallerLineNumber] int sourceLine = 0)
        {
            if (callee == nameof(InitiateConnectivityPingPoll))
            {

                PingPoll = new Ping();
                PingOptions pingOptions = new PingOptions() { DontFragment = false, Ttl = 1000 };
                PingPoll.PingCompleted += InternetConnectivityPollSync;
                PingPoll.SendAsync(PingAddress, 3000, Encoding.ASCII.GetBytes($"0x9826839"), pingOptions, new object());
                
            }
            else if (callee == nameof(InternetConnectivityPollSync))
            {
                pingThread = new Thread(() =>
                   {

                       Debug.WriteLine("Sending internet poll request .");

                       PingPoll.Dispose();
                       PingPoll = new Ping();

                       Thread.Sleep(TimeSpan.FromSeconds(5));

                       Connected = (PingPollReply.Status != IPStatus.DestinationHostUnreachable ||
                                                      PingPollReply.Status != IPStatus.BadRoute ||
                                 PingPollReply.Status != IPStatus.DestinationNetworkUnreachable ||
                                        PingPollReply.Status != IPStatus.DestinationUnreachable ||
                                                      PingPollReply.Status != IPStatus.TimedOut ||
                                 PingPollReply.Status != IPStatus.DestinationNetworkUnreachable ||
                        PingPollReply.Status != IPStatus.DestinationPortUnreachable) ? false : true;

                       PingOptions pingOptions = new PingOptions() { DontFragment = false, Ttl = 1000 };
                       PingPoll.PingCompleted += InternetConnectivityPollSync;
                       PingPoll.SendAsync(PingAddress, 3000, Encoding.ASCII.GetBytes($"0x9826839"), pingOptions, new object());
                       
                   }) { IsBackground = true };

                pingThread.Start();
                
            }
        }

            



        /// <summary> Initiate Pings to our backend server to confirm internet connectivity </summary>
        public bool InitiateConnectivityPingPoll()
        {
            ///Note: Only called once on app startup , gets the public ipaddress to ping .
            IpLookupSync();
            try
            {
                PingPoll = new Ping();
                PingPoll.PingCompleted += new PingCompletedEventHandler(InternetConnectivityPollSync);

                InternetConnectionPollSyncAsync();
                Debug.WriteLine("Initiating internet connection polls ");
            }
            catch (Exception Ex)
            {
                Debug.WriteLine(@"Connectivity Error : {0}", Ex); /// Wish there was a way to have custom excetption object while writing depending on what your code is trying to do .
                throw;
            }
            return true;
        }


        /// <summary> Triggered after ping request are done . </summary>
        /// <param name="sender">todo: describe sender parameter on InternetConnectivityPollSync</param>
        /// <param name="e">todo: describe e parameter on InternetConnectivityPollSync</param>
        private void InternetConnectivityPollSync(object sender, PingCompletedEventArgs e)
        {

            Debug.WriteLine("Polling internet connection");
            //Thread.Sleep(TimeSpan.FromSeconds(5));
            PingPollReply = e.Reply;
            InternetConnectionPollSyncAsync();
        }




    }
}
