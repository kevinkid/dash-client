﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Forms;
using System.Windows.Media.Animation;
using System.Diagnostics;

namespace Dash
{
    ///<summary>
    ///Interaction logic for DashMain.xaml
    // </summary
    public partial class DashMain : Window
    {
        public const double _originalHeight = 279.958;
        public double _tarHeight { get; set; }
        public double _toggleHeight { get; set; }
        Storyboard _storyboard;

        public DashMain()
        {
            InitializeComponent();
            Debug.WriteLine("Original Height :"+ this.Height);
        }

  
        public double toggleHieght()
        {
            _toggleHeight = ((_originalHeight + 230) < _toggleHeight+1) ? _originalHeight : (this.ActualHeight + 230);
            return _toggleHeight+1;
        }


        public void HeightAnimation()
        {
            _storyboard = new Storyboard();
            DoubleAnimation _dHeightAnimation = new DoubleAnimation();
            _dHeightAnimation.From = this.ActualHeight;
            _dHeightAnimation.To = toggleHieght();
            _dHeightAnimation.Duration = new Duration(TimeSpan.FromMilliseconds(200));
            _dHeightAnimation.AccelerationRatio = 1;
            Storyboard.SetTarget(_dHeightAnimation, this);
            Storyboard.SetTargetProperty(_dHeightAnimation, new PropertyPath(Window.HeightProperty));
            _storyboard.Children.Add(_dHeightAnimation);
            _storyboard.Begin();
        }


        // message button 
        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            System.Windows.Application.Current.MainWindow.Height = 300;
        }

        private void closeApp_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        /// Call button click event 
        private void callBtn_Click(object sender, RoutedEventArgs e)
        {
            HeightAnimation();
            //System.Windows.Application.Current.MainWindow.Height = 900;
        }

        ///TODO: Get the Windows acive theme and use it as a background 
        ///docs :https://msdn.microsoft.com/en-us/library/aa969513.aspx
        ///stackoverflow: http://stackoverflow.com/questions/13660976/get-the-active-color-of-windows-8-automatic-color-theme



    }
}
