﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Dash
{
    /// <summary>
    /// Interaction logic for MainView.xaml
    /// </summary>
    public partial class MainView : Window
    {
        public MainView()
        {
            InitializeComponent();
            MainFrame.Navigate(new Uri("/Components/Templates/GeneralSettings.xaml", UriKind.RelativeOrAbsolute));
        }
        
        private void GeneralSettings_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            MainFrame.Navigate(new Uri("/Components/Templates/GeneralSettings.xaml", UriKind.RelativeOrAbsolute));
        }

        private void AccountSettings_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            MainFrame.Navigate(new Uri("/Components/Templates/AccountsSettings.xaml", UriKind.RelativeOrAbsolute));
        }


        private void AlarmSettings_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            MainFrame.Navigate(new Uri("/Components/Templates/AlarmSettings.xaml", UriKind.RelativeOrAbsolute));
        }


        private void CalendarSettings_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            MainFrame.Navigate(new Uri("/Components/Templates/CalendarSettings.xaml", UriKind.RelativeOrAbsolute));
        }


        private void NotificationSettings_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            MainFrame.Navigate(new Uri("/Components/Templates/NotificationsSettings.xaml", UriKind.RelativeOrAbsolute));
        }


        private void CloseSettings_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            this.Hide();
        }
    }
}

