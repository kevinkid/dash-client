﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using MaterialDesignThemes.Wpf;
using System.Diagnostics;
using System.Threading;
using System.Windows.Threading;
using Dash;
using System.Reflection;


namespace Dash.Components.Templates
{
    /// <summary>
    /// Interaction logic for Accounts.xaml
    /// </summary>
    public partial class Accounts : UserControl
    {
        public string LastSelectAccount { get; set; }
        public Thread AccountListDisplay { get; set; }

        public static Dictionary<string, string> IconPaths = new Dictionary<string, string>()
        {
            { "gmail", "C:/Users/kevin/Documents/Visual Studio 2015/Projects/Notification-app-trial/Notification-app/Notification-app/Resources/icons/Gmail-icon.ico"},
            { "yammer", "C:/Users/kevin/Documents/Visual Studio 2015/Projects/Notification-app-trial/Notification-app/Notification-app/Resources/icons/yammer-acc-icon.png"},
            { "skype", "C:/Users/kevin/Documents/Visual Studio 2015/Projects/Notification-app-trial/Notification-app/Notification-app/Resources/icons/skype-acc-icon.png"},
            { "outlook", "C:/Users/kevin/Documents/Visual Studio 2015/Projects/Notification-app-trial/Notification-app/Notification-app/Resources/icons/outlook-icon.png"}
        };


        public Accounts()
        {
            InitializeComponent();
        }


        private void gmail_Selected(object sender, RoutedEventArgs e)
        {
            /// Note: This thread has to finish executing .
            gmail.Content = "Gmail";
            LastSelectAccount = "gmail";
        }


        private void yammer_Selected(object sender, RoutedEventArgs e)
        {
            /// Note: This thread has to finish executing .
            yammer.Content = "Yammer";
            LastSelectAccount = "yammer";
        }


        private void skype_Selected(object sender, RoutedEventArgs e)
        {
            /// Note: This thread has to finish executing .
            skype.Content = "Skype";
            LastSelectAccount = "skype";
        }


        private void outlook_Selected(object sender, RoutedEventArgs e)
        {
            /// Note: This thread has to finish executing .
            outlook.Content = "Outlook";
            LastSelectAccount = "outlook";
        }

        

        private async Task DisplayListIcon(string AccIcon)
        {
            Image accountImg = new Image()
            {
                Width = 100,
                Height = 50
            };
            
            AccountListDisplay = new Thread(
               (delegate ()
               {

                   SynchronizationContext.SetSynchronizationContext(new DispatcherSynchronizationContext(Dispatcher.CurrentDispatcher));

                   System.Reflection.PropertyInfo DispatcherInvokeProp = (typeof(Accounts) as Type)
                                                                         .GetProperty((string)AccIcon)
                                                                         .GetType().GetProperty(@"Dispatcher")
                                                                         .GetType().GetProperty(@"Invoke");


                   ChoosenAccount.Dispatcher.Invoke((delegate ()
                   {
                       BitmapImage GmailImgSrc = new BitmapImage();
                       GmailImgSrc.BeginInit();
                       GmailImgSrc.CreateOptions = BitmapCreateOptions.IgnoreColorProfile;
                       GmailImgSrc.UriSource = new Uri("C:/Users/kevin/Documents/Visual Studio 2015/Projects/Notification-app-trial/Notification-app/Notification-app/Resources/icons/Gmail-icon.ico", UriKind.RelativeOrAbsolute);
                       GmailImgSrc.EndInit();
                       accountImg.Source = GmailImgSrc;
                       gmail.Content = accountImg;

                       ///Note : The only reason for that is because i dont know how to reference inlined values returned like in lamba methods 
                       Debug.WriteLine((typeof(Accounts) as Type).GetProperty((string)AccIcon).GetType().GetProperty(@"Content"));
                       ///<debug> Make sure the above break above returns the image content property  </debug>

                       System.Reflection.PropertyInfo ImgProp = (typeof(Accounts) as Type)
                                                               .GetProperty((string)AccIcon)
                                                               .GetType().GetProperty(@"Content");
                       ImgProp.SetValue(ImgProp, accountImg);


                   }), DispatcherPriority.Background);



                   gmail.Dispatcher.Invoke(
                        (delegate ()
                        {
                            BitmapImage GmailImgSrc = new BitmapImage();
                            GmailImgSrc.BeginInit();
                            GmailImgSrc.CreateOptions = BitmapCreateOptions.IgnoreColorProfile;
                            GmailImgSrc.UriSource = new Uri("C:/Users/kevin/Documents/Visual Studio 2015/Projects/Notification-app-trial/Notification-app/Notification-app/Resources/icons/Gmail-icon.ico", UriKind.RelativeOrAbsolute);
                            GmailImgSrc.EndInit();
                            accountImg.Source = GmailImgSrc;
                            gmail.Content = accountImg;

                            ///Note : The only reason for that is because i dont know how to reference inlined values returned like in lamba methods 
                            Debug.WriteLine((typeof(Accounts) as Type).GetProperty((string)AccIcon).GetType().GetProperty(@"Content"));
                            ///<debug> Make sure the above break above returns the image content property  </debug>

                            System.Reflection.PropertyInfo ImgProp = (typeof(Accounts) as Type)
                                                                    .GetProperty((string)AccIcon)
                                                                    .GetType().GetProperty(@"Content");
                            ImgProp.SetValue(ImgProp, accountImg);
                            

                        }), DispatcherPriority.Background);

               }))
            {
                IsBackground = true
            };
            AccountListDisplay.SetApartmentState(ApartmentState.STA);
            AccountListDisplay.Start();
        }







        public async void ResetCombobox(string LastSelectedAccount)
        {
            object item = ChoosenAccount.SelectedItem;
            if (item != null)
            {

                Image accountImg = new Image()
                {
                    Width = 100,
                    Height = 50
                };

                switch (LastSelectedAccount)
                {
                    case "gmail":

                        gmail.Content = null;

                        //await DisplayListIcon(LastSelectedAccount);


                        /// Note: Find out if the thread takes alot of memory . 

                        AccountListDisplay = new Thread(
                        (delegate ()
                        {
                            SynchronizationContext.SetSynchronizationContext(new DispatcherSynchronizationContext(Dispatcher.CurrentDispatcher));
                            ChoosenAccount.Dispatcher.Invoke(
                                 (delegate ()
                                 {
                                     BitmapImage GmailImgSrc = new BitmapImage();
                                     GmailImgSrc.BeginInit();
                                     GmailImgSrc.CreateOptions = BitmapCreateOptions.IgnoreColorProfile;
                                     GmailImgSrc.UriSource = new Uri("C:/Users/kevin/Documents/Visual Studio 2015/Projects/Notification-app-trial/Notification-app/Notification-app/Resources/icons/Gmail-icon.ico", UriKind.RelativeOrAbsolute);
                                     GmailImgSrc.EndInit();
                                     accountImg.Source = GmailImgSrc;
                                     //gmail.Content = accountImg;

                                     /*
                                     ((System.Reflection.TypeInfo)((typeof(Accounts) as Type))).DeclaredFields.ToArray().AsEnumerable().Where((f) => f.Name == LastSelectedAccount);
                                     Debug.WriteLine("asd");

                                     Debug.WriteLine(((System.Reflection.TypeInfo)((typeof(Accounts) as Type))).DeclaredFields.ToArray());
                                     Debug.WriteLine(((System.Reflection.TypeInfo)((typeof(Accounts) as Type))).DeclaredFields.ToArray().Where((f) => f.Name == LastSelectedAccount).GetType().BaseType.Name);
                                     
                                     System.Reflection.PropertyInfo ImgProp = (typeof(Accounts) as Type).GetType()
                                                                              .GetProperty((string)LastSelectedAccount)
                                                                              .GetType().GetProperty(@"Content");
                                     ImgProp.SetValue(ImgProp, accountImg);
                                     */



                                     //== 

                                     Accounts acc = new Accounts();
                                     Debug.WriteLine("asdf");

                                     // Todo : iterate the array of propety info to get the control property .
                                     Debug.WriteLine(Convert.ChangeType(((System.Reflection.TypeInfo)((typeof(Accounts) as Type))).DeclaredFields.ToArray()[3], acc.GetType(), null));

                                     Debug.WriteLine(((System.Reflection.TypeInfo)((typeof(Accounts) as Type))).DeclaredFields.ToArray().Where((f) => f.Name == LastSelectedAccount).GetType().BaseType.Name);
                                    
                                    

                                     PropertyInfo propinfo = acc.GetType().GetProperty(LastSelectedAccount);
                                     propinfo.SetValue(acc, Convert.ChangeType(LastSelectedAccount, propinfo.PropertyType), null);
                                     
                                     //==
                                                
                        
                                 }), DispatcherPriority.Background);
                        
                        }));
                        AccountListDisplay.IsBackground = true;
                        AccountListDisplay.SetApartmentState(ApartmentState.STA);
                        AccountListDisplay.Start(); /// TODO: Dispose of the thread 




                        break;
                    case "yammer":

                        AccountListDisplay = new Thread(
                            (delegate ()
                            {

                                SynchronizationContext.SetSynchronizationContext(new DispatcherSynchronizationContext(Dispatcher.CurrentDispatcher));
                                yammer.Dispatcher.Invoke(
                                    (delegate ()
                                    {


                                        BitmapImage YammerImgSrc = new BitmapImage();
                                        YammerImgSrc.BeginInit();
                                        YammerImgSrc.UriSource = new Uri("C:/Users/kevin/Documents/Visual Studio 2015/Projects/Notification-app-trial/Notification-app/Notification-app/Resources/icons/yammer-acc-icon.png", UriKind.RelativeOrAbsolute);
                                        YammerImgSrc.EndInit();
                                        accountImg.Source = YammerImgSrc;
                                        yammer.Content = accountImg;

                                    }), DispatcherPriority.Background);

                            }));
                        AccountListDisplay.IsBackground = true;
                        AccountListDisplay.SetApartmentState(ApartmentState.STA);
                        AccountListDisplay.Start(); /// TODO: Dispose of the thread 


                        break;
                    case "skype":

                        AccountListDisplay = new Thread(
                            (delegate ()
                            {

                                SynchronizationContext.SetSynchronizationContext(new DispatcherSynchronizationContext(Dispatcher.CurrentDispatcher));
                                skype.Dispatcher.Invoke(
                                    (delegate ()
                                    {

                                        BitmapImage SkypeImgSrc = new BitmapImage();
                                        SkypeImgSrc.BeginInit();
                                        SkypeImgSrc.UriSource = new Uri("C:/Users/kevin/Documents/Visual Studio 2015/Projects/Notification-app-trial/Notification-app/Notification-app/Resources/icons/skype-acc-icon.png", UriKind.RelativeOrAbsolute);
                                        SkypeImgSrc.EndInit();
                                        accountImg.Source = SkypeImgSrc;
                                        skype.Content = accountImg;

                                    }), DispatcherPriority.Background);

                            }));
                        AccountListDisplay.IsBackground = true;
                        AccountListDisplay.SetApartmentState(ApartmentState.STA);
                        AccountListDisplay.Start(); /// TODO: Dispose of the thread                

                        break;
                    case "outlook":

                        AccountListDisplay = new Thread(
                            (delegate ()
                            {
                                SynchronizationContext.SetSynchronizationContext(new DispatcherSynchronizationContext(Dispatcher.CurrentDispatcher));

                                outlook.Dispatcher.Invoke(
                                    (delegate ()
                                    {

                                        BitmapImage OutlookImgSrc = new BitmapImage();
                                        OutlookImgSrc.BeginInit();
                                        OutlookImgSrc.UriSource = new Uri("C:/Users/kevin/Documents/Visual Studio 2015/Projects/Notification-app-trial/Notification-app/Notification-app/Resources/icons/outlook-icon.png", UriKind.RelativeOrAbsolute);
                                        OutlookImgSrc.EndInit();
                                        accountImg.Source = OutlookImgSrc;
                                        outlook.Content = accountImg;

                                    }), DispatcherPriority.Background);

                            }));
                        AccountListDisplay.IsBackground = true;
                        AccountListDisplay.SetApartmentState(ApartmentState.STA);
                        AccountListDisplay.Start(); /// TODO: Dispose of the thread 


                        break;
                    default:
                        break;
                }
                ChoosenAccount.SelectedItem = null;

            }
        }


        /// <summary>
        /// Reset ComboBoxItem 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ChoosenAccount_DropDownOpened(object sender, EventArgs e)
        {
            if (LastSelectAccount != null)
            {
                ResetCombobox(LastSelectAccount);
            }
        }


        /// <summary>
        /// Dispose of the last thread 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ChoosenAccount_Selected(object sender, RoutedEventArgs e)
        {
            try
            {
                //AccountListDisplay.Abort();///NOTE:  Turns out this doesnt exist in the current context, sounds like a problem , try aborting after starting . or using an async after modularizing . 
            }
            catch (ThreadAbortException ex)
            {
                Debug.WriteLine("Thread disposed ");
                Debug.WriteLine("Exception :" + ex);
            }
        }




        private void AddAccountBtn_Click(object sender, RoutedEventArgs e)
        {
            SessionManager signin = new SessionManager();
            if (LastSelectAccount == "outlook")
            {
                MainWindow authInstanceView = new MainWindow();
                signin.CreateSiginBrowser("outlook");
            }
            else if (LastSelectAccount == "gmail")
            {
                MainWindow authInstanceView = new MainWindow();
                signin.CreateSiginBrowser("outlook");
            }
            else if (LastSelectAccount == "yammer")
            {
                MainWindow authInstanceView = new MainWindow();
                signin.CreateSiginBrowser("outlook");

            }
            else if (LastSelectAccount == "skype")
            {
                MainWindow authInstanceView = new MainWindow();
                signin.CreateSiginBrowser("outlook");
            }
        }



    }
}
