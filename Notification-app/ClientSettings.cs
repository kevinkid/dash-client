﻿using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.Configuration;


namespace Dash
{
    /// <summary>
    /// Documentation: https://msdn.microsoft.com/en-us/library/system.configuration.configurationmanager.appsettings(v=vs.110).aspx
    /// </summary>
    class ClientSettings : ApplicationSettingsBase
    {
        ///<summary> Defualt settings  </summary>
        public static bool LastTimeConnected { get; set; }
        public static string SessionKey { get; set; }
        public object ConnectedAccounts { get; set; }


        /// <summary> Sets the application settings properties on startup such that we are not reading from the config file everytime .</summary>
        public static void StartupSetttings()
        {
            ///Note : Assign the settings from the config file to the static properties to make it easier to query user settings .
        }

        /// <summary>  Set the user settings on a new account or install instance . </summary>
        /// <param name="settingskey">todo: describe settingskey parameter on SetAppSettings</param>
        /// <param name="settingsvalue">todo: describe settingsvalue parameter on SetAppSettings</param>
        public bool SetAppSettings(string settingskey, string settingsvalue)
        {
            Debug.WriteLine(@"Executable Path: {0}", System.Reflection.Assembly.GetExecutingAssembly().Location.ToString());
            string ExecPath = string.Format(@"{0}", (string)System.Reflection.Assembly.GetExecutingAssembly().Location);

            /// Docs: https://msdn.microsoft.com/en-us/library/system.configuration.configurationelement.lockallattributesexcept(v=vs.110).aspx
            bool locked = false;
            var config = ConfigurationManager.OpenExeConfiguration(ExecPath);
            var appSettings = config.AppSettings.Settings;
            if (settingskey != null && settingsvalue != null)
            {
                if (GetAppSetting(settingskey)[settingskey] == null)
                {
                    appSettings.Add(settingskey, settingsvalue);
                    config.Save(ConfigurationSaveMode.Full, true);
                    ConfigurationManager.RefreshSection(config.AppSettings.SectionInformation.Name);
                }
                else
                {
                    UpdateSettings(settingskey, settingsvalue);
                }
            }
            return !locked;
        }


        /// <summary> Query user settings using a specific key . </summary>
        /// <param name="settingsKey">todo: describe settingsKey parameter on GetAppSetting</param>
        public Dictionary<string, object> GetAppSetting(string settingsKey)
        {
            Debug.WriteLine(@"Executable Path: {0}", System.Reflection.Assembly.GetExecutingAssembly().Location.ToString());
            string ExecPath = string.Format(@"{0}", (string)System.Reflection.Assembly.GetExecutingAssembly().Location);

            var appSettings = ConfigurationManager.AppSettings;
            string settingsValue = null;
            Dictionary<string, object> resultSetting = new Dictionary<string, object>();
            if (settingsKey != null)
                settingsValue = appSettings.Get(settingsKey) ?? null;
            resultSetting.Add(settingsKey, settingsValue);
            return resultSetting;
        }


        /// <summary>  Retrive all the current user settings . </summary>
        public static List<object> GetAppSettings()
        {
            Debug.WriteLine(@"Executable Path: {0}", System.Reflection.Assembly.GetExecutingAssembly().Location.ToString());
            string ExecPath = string.Format(@"{0}", (string)System.Reflection.Assembly.GetExecutingAssembly().Location);
            var appSettings = ConfigurationManager.AppSettings;
            List<object> settings = new List<object>();
            foreach (var settingsItem in appSettings.AllKeys)
            {
                settings.Add(settingsItem);
                Debug.WriteLine(@"Settings: [ {0} : {1} ]", settingsItem, appSettings.GetValues(settingsItem));
            }
            return settings;
        }

        
        /// <summary> Update user settings  </summary>
        public static bool UpdateSettings(string key, string value)
        {
            bool locked = false;
            string ExecPath = string.Format(@"{0}", (string)System.Reflection.Assembly.GetExecutingAssembly().Location);

            var config = ConfigurationManager.OpenExeConfiguration(ExecPath);
            var appSettings = config.AppSettings.Settings;
            if (key != null && value != null)
            {
                if (!(appSettings.IsReadOnly()))
                {
                    if (appSettings[key] != null)
                    {
                        appSettings[key].Value = value;
                        config.Save(ConfigurationSaveMode.Full, true);
                    }
                }
            }
            return !locked;
        }



    }
}
