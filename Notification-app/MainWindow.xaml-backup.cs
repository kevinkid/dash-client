﻿//using System;
//using System.Collections.Generic;
//using System.Diagnostics;
//using System.Linq;
//using System.Runtime.InteropServices;
//using System.Text;
//using System.Windows;
//using System.Windows.Controls;
//using System.Windows.Data;
//using System.Windows.Documents;
//using System.Windows.Input;
//using System.Windows.Media;
//using System.Windows.Media.Imaging;
//using System.Windows.Navigation;
//using System.Windows.Shapes;
//using System.Timers;
//using System.Windows.Threading;
//using System.Windows.Interop;
//using System.Web;
//using System.IO;
//using System.Collections.Specialized;
//using Telerik.Windows.Controls;
//using Telerik.Windows.Controls.Animation;
//using System.Net.Sockets;
//using System.Threading.Tasks;
//using System.Threading;
//using Microsoft.AspNet.SignalR.Client;



//namespace Notification_app
//{


//    /// <summary>
//    /// Application Interface Interaction logic 
//    /// </summary>
//    public partial class MainWindow : Window
//    {
//        // Views
//        private MainView mainView;
//        private View view;
//        private MainView DefaultView;

//        // compilation error 
//        double screenWResolution = System.Windows.SystemParameters.PrimaryScreenWidth;
//        double screenHResolution = System.Windows.SystemParameters.PrimaryScreenHeight;


//        private List<System.Windows.Window> WinHandles;/// TODO: use this find all the windows there apps we want our app on top of.
//        private System.Windows.Window winHandles;
//        private static DispatcherTimer ActiveWindowChecker;
//        private static DispatcherTimer ClockTimer;


//        public Dictionary<string, string> client = new Dictionary<string, string>();
//        public IHubProxy HubProxy { set; get; }
//        const string ServerURI = "http://localhost:3000";
//        public HubConnection HubConnect { set; get; }



//        public bool signedin = false;
//        private System.Windows.Window Authwindow;
//        WebBrowser AuthBrowser = new WebBrowser();

//        public RadDesktopAlertManager AlertGroupManager { get; set; }
//        public RadDesktopAlertManager SingleAlertManager { get; set; }
//        public RadDesktopAlert Alert { get; set; }
//        public static object HttpContext { get; private set; }

//        public MainWindow()
//        {

//            InitializeComponent();

//            this.mainView = new MainView();
//            view = new View();
//            DefaultView = new MainView();
//            /// TODO: Embed this credential declarations on the app.config file then use the app.resources API .
//            Application.Current.Properties.Add("TenantID", "b9b034ce-8927-412d-9151-a9b1a1526d60");
//            Application.Current.Properties.Add("ClientID", "d2d6267b-a005-4146-b79a-a754e5e0def3");
//            Application.Current.Properties.Add("Client_Secret", "wLPDOaGMOjWtGu9iUzsWsMcOLrXPwNG9uOdswFFQoj0=");

//            Authwindow = new System.Windows.Window();
//            Authwindow.WindowStartupLocation = System.Windows.WindowStartupLocation.CenterScreen;
//            Authwindow.Height = 700;
//            Authwindow.Width = 889;
//            Authwindow.Uid = "Dash.AuthenticationWindow";
//            Authwindow.Topmost = true;
//            Authwindow.Title = "Signin with our Office 365 Credentials";
//            AuthBrowser.Height = Authwindow.Height;
//            AuthBrowser.Width = Authwindow.Width;
//            Authwindow.Content = AuthBrowser;

//            HubConnect = new HubConnection(ServerURI);
//            HubConnect.Closed += HubClosedConnection;
//            HubProxy = HubConnect.CreateHubProxy("MyHub");//[DashHubProxy] note:this should match on the server side .
//            HubConnect.Credentials = System.Net.CredentialCache.DefaultCredentials;
//            HubConnect.ConnectionSlow += ConnectionTimeoutHandle;
//            HubConnect.Reconnected += OnReconnect;

//            //SignalR Debug output 
//            HubConnect.TraceLevel = TraceLevels.All;
//            HubConnect.TraceWriter = Console.Out;

//            HubConnect.Received += IncommingNotification;


//            ClockTimer = new DispatcherTimer(DispatcherPriority.SystemIdle);
//            ClockTimer.Tick += UpdateClock;

//            ClockTimer.Interval = TimeSpan.FromSeconds(1);
//            ClockTimer.Start();
//            ActiveWindowChecker = new DispatcherTimer(DispatcherPriority.SystemIdle);
//            ActiveWindowChecker.Tick += new EventHandler(ActiveWindowHandle);

//            ActiveWindowChecker.Interval = TimeSpan.FromSeconds(1);
//            ActiveWindowChecker.Start();

//            Debug.WriteLine(screenWResolution);
//            Debug.WriteLine(screenHResolution);
//            Debug.WriteLine("Os " + System.Environment.OSVersion); // NOTE: Unrealiable data eg. Os Microsoft Windows NT 6.2.9200.0 [win10]- used for the blur window effect.


//            this.AlertGroupManager = new RadDesktopAlertManager(AlertScreenPosition.BottomRight);
//            this.SingleAlertManager = new RadDesktopAlertManager(AlertScreenPosition.BottomRight);
//            /***********[   rad desktop alert         ]***********/

//            bool isLargeScreen = ((screenHResolution > 1200) && (screenWResolution < 1500)) ? !(SmallScreen()) : LargeSCreen();


//        }

//        private void Window_Loaded(object sender, RoutedEventArgs e)
//        {

//        }


//        private void OnReconnect()
//        {
//            // Recheck credentials 
//        }


//        // Retry connection to the server .
//        private void ConnectionTimeoutHandle()
//        {
//            InitConnectionSync();
//        }



//        /// Meduim Screen Size 
//        private bool SmallScreen()
//        {
//            this.Left = screenWResolution - screenWResolution / 2 + 90;// was 80
//            this.Top = screenHResolution - screenHResolution + 50;
//            return true;
//        }

//        /// Larger Screen Size 
//        private bool LargeSCreen()
//        {
//            this.Left = screenWResolution - screenWResolution / 3 + 80;
//            this.Top = screenHResolution - screenHResolution + 20;
//            return true;
//        }


//        ///<summary>
//        /// Scan active window to change the application's state 
//        /// If the application is the only application window open then it becomes the topmost 
//        /// NOTE: Optionally you can just launch the application in specific place and it is 
//        /// not movable it becomes the top most only to the desktop only . 
//        /// </summary>
//        private void ActiveWindowHandle(object sender, EventArgs e)
//        {

//            /// TODO: Get all the windows open then Change the [this.Topmost] value to true if there is no active window open
//            System.Windows.Window hndl;
//            hndl = Application.Current.Windows.OfType<Window>().SingleOrDefault(x => x.IsActive);///http://stackoverflow.com/questions/2038879/refer-to-active-window-in-wpf
//            winHandles = hndl;
//            Debug.WriteLine(hndl);

//        }

//        /// <summary>
//        /// Updates the clock on the newest time info 
//        /// NOTE: Better time in on the UI project merge it to get the updated method .
//        /// </summary>
//        /// <param name="sender"></param>
//        /// <param name="e"></param>
//        private void UpdateClock(object sender, EventArgs e)
//        {
//            //TODO: Find out which is better internet time or the machine time , i think internet time is better because it changes with location .
//            // Update Time 
//            string time = DateTime.Now.ToString("h:mm:ss tt");
//            string TimeOfDay = (time.IndexOf("PM") > 0) ? "PM" : "AM";
//            // Update Date 
//            string day = DateTime.Today.DayOfWeek.ToString();
//            string date = DateTime.Now.ToString("dd");
//            string month = DateTime.Now.ToString("MMMM");
//            string year = DateTime.Now.ToString("yyyy");


//            time = time.Remove((time.IndexOf("PM") > 0) ? time.IndexOf("PM") - 4 : time.IndexOf("AM") - 4, 6);
//            this.mainView.label.Content = time;
//            this.mainView.label1.Content = TimeOfDay;
//            string DateFinal = "";

//            DateFinal += date + "  ";
//            DateFinal += month + " ";
//            DateFinal += year;

//            this.mainView.label5.Content = day;
//            this.mainView.label6.Content = DateFinal;



//        }



//        /// <summary>
//        /// Trigger a popup webview to get authetication token 
//        /// </summary>
//        /// <param name="sender"></param>
//        /// <param name="e"></param>
//        private void loginBtn_Click(object sender, RoutedEventArgs e)
//        {

//            if (!signedin)
//            {
//                //shows permissions scope prompt "https://login.windows.net/common/oauth2/authorize?response_type=code&resource=https%3A%2F%2Fmanage.office.com&client_id=d2d6267b-a005-4146-b79a-a754e5e0def3&redirect_uri=https://login.live.com/oauth20_desktop.srf";
//                string AuthUri = String.Format(@"https://login.microsoftonline.com/{0}/oauth2/authorize?client_id={1}&response_type=code&redirect_uri=https://dash-testbed.herokuapp.com", Application.Current.Properties["TenantID"], Application.Current.Properties["ClientID"]);
//                Authwindow.Show();
//                AuthBrowser.Navigate(AuthUri);
//                AuthBrowser.Navigated += captureCode;

//            }
//            else
//            {
//                /// TODO: get the authentication token and update the UI 
//                ResetCredentialSync();
//            }

//        }




//        private void captureCode(object sender, NavigationEventArgs e)
//        {
//            //Note: The same way you are capturing the code we are going to also capture the 
//            //parameters and use the urlparser to get a json object with credentails inside .
//            // Since we are going to make a javascript implementation of the same where we are expected 
//            // to get the code then make a post requesting for an access token, 
//            // var search = location.search.substring(1);
//            //JSON.parse('{"' + decodeURI(search).replace(/ "/g, '\\"').replace(/&/g, '","').replace(/=/g,'":"') + '"}')
//            //http://stackoverflow.com/questions/8648892/convert-url-parameters-to-a-javascript-object
//            // Our redirect url will have session or/and permanent credentails what we are going to associate with the client. as above
//            //var search = location.search.substring(1);
//            //search? JSON.parse('{"' + search.replace(/ &/ g, '","').replace(/=/ g, '":"') + '"}',
//            //                  function(key, value) { return key === "" ? value : decodeURIComponent(value) }):{ }

//            Debug.WriteLine("url contain code");
//            Debug.WriteLine(e.Uri.PathAndQuery.Contains("code"));//TODO: Change to SessionKey 

//            if (e.Uri.PathAndQuery.Contains("code="))
//            {
//                string rURL = e.Uri.PathAndQuery;
//                //TODO: Check for a session token.
//                Debug.WriteLine("Authentication code !");
//                Debug.WriteLine(e.Uri.PathAndQuery);
//                Authwindow.Close();
//                /// Trim url to retrive the Auth code   
//                rURL = rURL.Remove(0, rURL.IndexOf("code=") + 5);
//                rURL = rURL.Remove(rURL.IndexOf("&session_state="), rURL.Length - rURL.IndexOf("&session_state="));
//                Application.Current.Properties.Add("AuthCode", rURL);
//                Debug.WriteLine(rURL);

//            }

//        }

//        private static Dictionary<string, string> UriParser(string url)

//        {
//            //https://msdn.microsoft.com/en-us/library/system.web.httputility.urldecode(v=vs.110).aspx
//            string decodedUrl = Uri.UnescapeDataString(url);

//            //TODO: Parse the url to get the object values of the 
//            Dictionary<string, string> _cred = new Dictionary<string, string>();
//            _cred.Add("smekthi", "asdf");
//            return _cred;

//        }

//        /// <summary>
//        /// Refresh credentails just incase of instances where
//        /// the user restarted computer ...
//        /// </summary>
//        private async void ResetCredentialSync()
//        {

//        }

//        /// <summary>
//        /// Browser Prompt for credentails and sync accounts .
//        /// </summary>
//        private async void InitConnectionSync()
//        {
//            //TODO: Find ways to use the onNotify as a way to filter who get what messages 
//            // note: maybe after successful connection cange change a certain property that 
//            // is equal to the socket token which we us to identify a client and then the server 
//            // matches that to the account to take notification from . 

//            /*
//                client.Add("SessionToken", "zero");
//                string token = client["SessionToken"];
//                // note: the tokenkey changes after successful socken token connection, hopefully that is the same with what is in the 
//                // This might now work as well because the method name is stored maybe .
//                HubProxy.On<string, string>(tokenkey,(name,message) => {
//                    Debug.WriteLine("Sure incoming intended message what we don't have to write code for . ");
//                });
//            */

//            // set routes 
//            HubProxy.On<string, string>("OnNotification", (name, message) =>
//            {
//                this.Dispatcher.Invoke(() =>
//                {
//                    Debug.WriteLine("Invoked on incoming message . ");
//                });
//            });

//            HubProxy.On<string, string>("AddMessage", (Name, message) =>
//            {
//                Debug.WriteLine("Message recieved from server .");
//            });

//            await HubConnect.Start();

//        }

//        private void IncommingNotification(string obj)
//        {
//            Debug.WriteLine("Notification recieved !");

//        }

//        private void HubClosedConnection()
//        {
//            Debug.WriteLine("Hub connection closed .");
//        }



//        //Close/minimise button 
//        private void minimizeBtn_Click(object sender, RoutedEventArgs e)
//        {
//            /// TODO: Minimize application and take to the background 
//            Application.Current.Shutdown();
//        }

//        /// Lock button 
//        private void lockBtn_Click(object sender, RoutedEventArgs e)
//        {


//        }

//        // Show Desktop button
//        private void Show_Alert_click(object sender, RoutedEventArgs e)
//        {

//            this.Alert = new RadDesktopAlert();
//            this.Alert.Header = "Demo headline title";
//            this.Alert.Content = "Demo alert content";
//            BitmapImage MsgIcon = new BitmapImage();
//            MsgIcon.UriSource = new Uri("Resources/icons/twitter-icon.png", UriKind.RelativeOrAbsolute);

//            this.Alert.Icon = MsgIcon;
//            this.Alert.IconColumnWidth = 20;
//            this.Alert.ShowDuration = 3000;// time[ms]

//            var slideInAnimation = new SlideAnimation
//            {
//                Direction = AnimationDirection.In,
//                Orientation = Orientation.Horizontal,
//                SpeedRatio = 0.5d
//            };

//            var slideOutAnimation = new SlideAnimation
//            {
//                Direction = AnimationDirection.Out,
//                Orientation = Orientation.Horizontal,
//                SpeedRatio = 0.5d
//            };

//            this.AlertGroupManager.ShowAnimation = slideInAnimation;
//            this.SingleAlertManager.HideAnimation = slideOutAnimation;

//            this.SingleAlertManager.ShowAlert(this.Alert);

//        }

//        //Hide Desktop button 
//        private void Hide_Alert_Click(object sender, RoutedEventArgs e)
//        {
//            this.AlertGroupManager.CloseAllAlerts();
//            this.SingleAlertManager.CloseAllAlerts();
//        }

//        // Connection button 
//        private void button2_Click(object sender, RoutedEventArgs e)
//        {
//            if (!signedin)
//            {
//                // Maybe the app was minimised to the background
//                InitConnectionSync();

//            }
//            else
//            {
//                ResetCredentialSync();
//            }

//        }


//        private void View_Loaded(object sender, RoutedEventArgs e)
//        {
//            view.RadTransitionControl.Content = DefaultView;
//        }
//    }
//}
