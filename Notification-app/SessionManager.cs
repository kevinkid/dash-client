﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using System.Windows.Controls;
using System.Windows.Navigation;
using System.Web;
using System.Windows;


namespace Dash
{
    class SessionManager
    {
        ClientSettings settings;
        public string SessionKey { get; set; }
        private  int AuthWindowH = 700;
        private int AuthWindowW = 889;

        WebBrowser AuthBrowser;
        System.Windows.Window Authwindow;
        
        public Dictionary<string, string> SigninUrls = new Dictionary<string, string>()
        {
            { "outlook" , "https://dashdesk.azurewebsites.net/login/outlook" },
            { "gmail", "https://dashdesk.azurewebsites.net/login/gmail" },
            { "yahoo", "https://dashdesk.azurewebsites.net/login/yahoo" },
            { "skype", "https://dashdesk.azurewebsites.net/login/skype" },
            { "asana", "https://dashdesk.azurewebsites.net/login/asana" },
            { "evernote", "https://dashdesk.azurewebsites.net/login/evernote" },
            { "slack", "https://dashdesk.azurewebsites.net/login/slack" }
        };



        public void CreateSiginBrowser(string AccountType)
        {
            AuthBrowser = new WebBrowser()
            {
                Height = AuthWindowH,
                Width = AuthWindowW
            };
            Authwindow = new System.Windows.Window() {
                Height = AuthWindowH,
                Width = AuthWindowW,
                Topmost = true,
                ResizeMode = ResizeMode.CanResize,
                WindowStartupLocation = WindowStartupLocation.CenterOwner,
                Uid = string.Format(@"Dash.{0}Signin", AccountType),
                Title = string.Format(@"Signin with your {0} Credentails",AccountType),
                Content = AuthBrowser
            };

            AuthBrowser.Navigated += AuthBrowser_Navigated;
            string AuthUri = "https://dashdesk.azurewebsites.net";
            Authwindow.Show();
            AuthBrowser.Navigate(AuthUri);

        }

        /// <summary>  Capture SessionKey credentials on page navigation </summary>
        private void AuthBrowser_Navigated(object sender, NavigationEventArgs e)
        {
            /// Capture sessionCredentials 
            if (e.Uri.PathAndQuery.Contains("subscriptionId="))
            {
                var rURL = new Uri(new Uri(e.Uri.AbsoluteUri).ToString());
                var query = HttpUtility.ParseQueryString(rURL.Query);
                string subscriptionId = query.Get("subscriptionId");
                Application.Current.Properties["subscriptionId"] = subscriptionId;
                Debug.WriteLine(rURL);
                Authwindow.Hide();
                settings = new ClientSettings();
                settings.SetAppSettings("subscriptionId", subscriptionId);
            }
        }


        /// <summary> Refresh credentails just incase of instances where the user restarted computer ... </summary>
        public async void ResetCredentialSync()
        {

        }



        public bool VerifySigin(string Account)
        {
            return true;
        }


        public void CreateSigninSync(string Account)
        {
            switch (Account)
            {
                case "Outlook": OutlookSignin(); break;
                case "Gmail": GmailSignin(); break;
                case "Yahoo": YahooSignin(); break;
                case "Skype": SkypeSigin(); break;
                case "Yammer": YammerSigin(); break;
                case "Asana": AsanaSignin(); break;
                case "Evernote": EvernoteSigin(); break;
                case "Slack": SlackSignin(); break;
            }
        }

        /// <summary>  Signin implement for different accounts types  </summary>
        private void OutlookSignin()
        {
            if (!VerifySigin("outlook"))
                CreateSiginBrowser("Outlook");
                Debug.WriteLine("outlook sigin ... ");
        }

        public void GmailSignin()
        {
            if (!VerifySigin("gmail"))
                CreateSiginBrowser("Gmail");
                Debug.WriteLine("Gmail sigin ... ");
        }

        public void YahooSignin()
        {
            if (!VerifySigin("yahoo"))
                CreateSiginBrowser("Yahoo");
                Debug.WriteLine("yahoo sigin ... ");
        }

        public void YammerSigin()
        {
            if (!VerifySigin("yammer"))
                CreateSiginBrowser("Yammah");
                Debug.WriteLine("yammer sigin ... ");
        }

        public void SkypeSigin()
        {
            if (!VerifySigin("Skype"))
                CreateSiginBrowser("Skype");
                Debug.WriteLine("Skype sigin ... ");
        }


        private void AsanaSignin()
        {
            if (!VerifySigin("asana"))
                CreateSiginBrowser("Asana");
                Debug.WriteLine("asana sigin ... ");
        }

        private void EvernoteSigin()
        {
            if (!VerifySigin("evernote"))
                CreateSiginBrowser("Evernote");
                Debug.WriteLine("evernote sigin ... ");
        }

        private void SlackSignin()
        {
            if (!VerifySigin("slack"))
                CreateSiginBrowser("Slack");
                Debug.WriteLine("slack sigin ... ");
        }

        public bool isOnline()
        {
            ///TODO: Handle offline application state better .
            return true;
        }



    }
}
